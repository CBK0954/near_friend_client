import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_NEAR_FRIENDS]: (store, payload) => {
        return axios.post(apiUrls.DO_NEAR_FRIENDS, payload)
    },
}
